extends Button

func _ready() -> void:
	# can't quit in web build
	if OS.has_feature("web"):
		queue_free()
	connect('pressed', self, 'quit_game')

func quit_game() -> void:
	get_tree().quit()
