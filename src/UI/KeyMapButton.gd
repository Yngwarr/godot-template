extends Button

var catcher: ConfirmationDialog
var event: InputEvent
var action: String

func _ready() -> void:
	connect('pressed', self, 'show_catcher')

func init(action_name: String, input_event: InputEvent, key_catcher: ConfirmationDialog) -> void:
	set_action(action_name, input_event)
	set_catcher(key_catcher)

func set_action(name: String, e: InputEvent) -> void:
	action = name
	event = e
	text = Global.event_as_text(e)

func set_catcher(key_catcher: ConfirmationDialog):
	catcher = key_catcher
	catcher.connect('action_remapped', self, 'update_info')

func show_catcher() -> void:
	catcher.set_action(action, event)
	catcher.popup_centered()

func update_info(a: String, current: InputEvent, past: InputEvent) -> void:
	if event != past || a != action: return
	event = current
	text = Global.event_as_text(current)
